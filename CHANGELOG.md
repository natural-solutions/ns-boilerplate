## [1.0.6](https://gitlab.com/natural-solutions/ns-boilerplate/compare/v1.0.5...v1.0.6) (2021-05-11)


### Bug Fixes

* add option --no-git-tag-version ([af029b6](https://gitlab.com/natural-solutions/ns-boilerplate/commit/af029b666fb2d4692dc34c2365635d51361a05ac))
* change step ([5d8d82d](https://gitlab.com/natural-solutions/ns-boilerplate/commit/5d8d82d6aadd255f3059a6f22a92f576192f7573))

## [1.0.5](https://gitlab.com/natural-solutions/ns-boilerplate/compare/v1.0.4...v1.0.5) (2021-05-11)


### Bug Fixes

* add package ([cb07d4e](https://gitlab.com/natural-solutions/ns-boilerplate/commit/cb07d4e3f8b631f1c059fc4349b042185add0211))

## [1.0.4](https://gitlab.com/natural-solutions/ns-boilerplate/compare/v1.0.3...v1.0.4) (2021-05-11)


### Bug Fixes

* changes on changelog are commited ([1835567](https://gitlab.com/natural-solutions/ns-boilerplate/commit/1835567bf62a139e535253b6aff8dc22f68479b5))

## [1.0.1](https://gitlab.com/natural-solutions/ns-boilerplate/compare/v1.0.0...v1.0.1) (2021-05-11)


### Bug Fixes

* start version from 1.0.0 ([21aa129](https://gitlab.com/natural-solutions/ns-boilerplate/commit/21aa129b5b8255560fa7cdc56def6123fcc8c423))

# 1.0.0 (2021-05-11)


### Features

* add semantic-release to boilerplate ([6a852e2](https://gitlab.com/natural-solutions/ns-boilerplate/commit/6a852e235d7b0f9dd304c84b7ad6d874b62dd70d))
