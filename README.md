# NS COMMITLINT SEMANTIC-RELEASE

Template is working
BUT

you have to change the package.json for you repositories:

- name
- version
- bugs.url
- homepage

```js
{
  "name": "name", //change the name
  "version": "1.0.5",//change the version
  "description": "none",
  "scripts": {
    "prepare": "husky install",
    "install": "npm run prepare",
    "release": "semantic-release"
  },
  "repository": {
    "type": "git",
    "url": "git+ssh://git@gitlab.com/natural-solutions/ns-boilerplate.git" //change the url
  },
  "author": "",
  "license": "ISC",
  "bugs": {
    "url": "https://gitlab.com/natural-solutions/ns-boilerplate/issues" //change the url
  },
  "homepage": "https://gitlab.com/natural-solutions/ns-boilerplate#readme", //change the url
  "devDependencies": {
    "@commitlint/cli": "^12.1.1",
    "@commitlint/config-conventional": "^12.1.1",
    "@semantic-release/changelog": "^5.0.1",
    "@semantic-release/commit-analyzer": "^8.0.1",
    "@semantic-release/exec": "^5.0.0",
    "@semantic-release/git": "^9.0.0",
    "@semantic-release/gitlab": "^6.0.9",
    "@semantic-release/release-notes-generator": "^9.0.2",
    "husky": "^6.0.0",
    "semantic-release": "^17.4.2"
  }
}
```
